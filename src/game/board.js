import CellState from './cell-state';

export default class Board {
  constructor(size) {
    this.boardSize = size;
    this.state = [];

    for (let i = 0; i < size; i++) {
      const row = [];
      for (let j = 0; j < size; j++) {
        row.push(CellState.EMPTY);
      }
      this.state.push(row);
    }
  }
}