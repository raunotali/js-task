import CellState from './cell-state';

const UI = {
  drawBoard(board, xTurn) {

    const boardSize = board.boardSize;
    const boardElement = document.querySelector(".board");
    const playerTurnElement = document.querySelector("[data-player]");
    const boardSide = window.innerHeight < window.innerWidth ? window.innerHeight / 2 : window.innerWidth / 2;

    boardElement.style.gridTemplate = `repeat(${boardSize}, 1fr) / repeat(${boardSize}, 1fr)`;

    playerTurnElement.innerHTML = xTurn ? CellState.X : CellState.O;

    boardElement.style.height = `${boardSide}px`;
    boardElement.style.width = `${boardSide}px`;

    for (let y = 0; y < board.boardSize; y++) {
      for (let x = 0; x < board.boardSize; x++) {
        const cell = document.querySelector(`[data-coordinates='${y}-${x}']`);
        cell.innerHTML = board.state[y][x];
      }
    }
    

  }
}

export { UI as default };