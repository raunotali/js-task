const CellState = {
  EMPTY: "",
  X: "X",
  O: "O"
}

export {CellState as default};