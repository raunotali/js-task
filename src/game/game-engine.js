import Board from './board';
import UI from './game-ui';
import CellState from './cell-state';

export default class Game {
  constructor(boardSize) {
    this.board = new Board(boardSize);
    this.xTurn = true;
  }

  gameOver() {

    const gameInfoElement = document.querySelector(".game-info");

    if (this.winningContitonIsMet()) {
      gameInfoElement.innerHTML = `Player ${!this.xTurn ? CellState.X : CellState.O} won`;
      return true;
    } else if (this.boardIsFull()) {
      gameInfoElement.innerHTML = 'Draw!';
      return true;
    }

    return false;
  }

  makeAMove(element) {

    const [y, x] = element.dataset.coordinates.split("-");

    if (this.board.state[y][x] !== CellState.EMPTY) {
      return;
    }

    this.board.state[y][x] = this.xTurn ? CellState.X : CellState.O;

    this.xTurn = !this.xTurn;
    UI.drawBoard(this.board, this.xTurn);

    if (this.gameOver()) {
      document.body.replaceWith(document.body.cloneNode(true));
      return;
    }
  }

  initializeGame() {
    const boardSize = this.board.boardSize;
    const boardElement = document.querySelector(".board");

    for (let y = 0; y < boardSize; y++) {
      for (let x = 0; x < boardSize; x++) {
        const cell = document.createElement("div");
        cell.classList.add("cell");
        cell.dataset.coordinates = `${y}-${x}`;

        cell.addEventListener('click', (event) => {
          this.makeAMove(event.target);
        })

        boardElement.appendChild(cell);
      }
    }

    window.addEventListener('resize', () => {
      UI.drawBoard(this.board, this.xTurn)
    }
    );

    UI.drawBoard(this.board, this.xTurn);
  }

  winningContitonIsMet() {

    const playerState = !this.xTurn ? CellState.X : CellState.O;
    let count = 0;
    //check rows
    for (let y = 0; y < this.board.boardSize; y++) {
      for (let x = 0; x < this.board.boardSize; x++) {
        if (this.board.state[y][x] === playerState) {
          count++;
        }
      }

      if (count === this.board.boardSize) {
        return true;
      }
      count = 0;
    }

    //check cols
    for (let x = 0; x < this.board.boardSize; x++) {
      for (let y = 0; y < this.board.boardSize; y++) {
        if (this.board.state[y][x] === playerState) {
          count++;
        }
      }

      if (count === this.board.boardSize) {
        return true;
      }
      count = 0;
    }

    //check neg diag
    for (let i = 0; i < this.board.boardSize; i++) {
      if (this.board.state[i][i] === playerState) {
        count++;
      }
    }

    if (count === this.board.boardSize) {
      return true;
    }
    count = 0;

    //check pos diag
    for (let i = 0; i < this.board.boardSize; i++) {
      if (this.board.state[this.board.boardSize - 1 - i][i] === playerState) {
        count++;
      }
    }

    if (count === this.board.boardSize) {
      return true;
    }

    return false;
  }

  boardIsFull() {

    for (let y = 0; y < this.board.boardSize; y++) {
      for (let x = 0; x < this.board.boardSize; x++) {
        if (this.board.state[y][x] === CellState.EMPTY) {
          return false;
        }
      }
    }
    return true;
  }

}